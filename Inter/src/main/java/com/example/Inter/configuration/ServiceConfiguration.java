package com.example.Inter.configuration;

import com.example.Inter.service.EncryptService;
import com.example.Inter.service.UniqueTypeService;
import com.example.Inter.service.UserService;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;

@Configuration
public class ServiceConfiguration {

    @Bean
    public com.example.Inter.service.UserService userService(){
        return new UserService();
    }


    @Bean
    public com.example.Inter.service.UniqueTypeService uniqueTypeService(){
        return new UniqueTypeService();
    }

    @Bean
    public com.example.Inter.service.EncryptService encryptService(){
        return new EncryptService();
    }

}